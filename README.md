# JavaScript Calculator

JavaScript Calculator est une calculatrice construite à l'aide de React utilisable depuis un navigateur web

## Installation

Vous pouvez accéder au projet directement en suivant se lien :

Ou vous pouvez cloner le dépôt en utilisant git clone. Il vous faudra utiliser ensuite git install pour pouvoir utiliser l'application

```bash
git clone https://gitlab.com/mateusbubono/javascript-calculator.git
cd javascript-calculator
npm install
npm run build
```

## Usage

Depuis le répertoire du projet utiliser npm pour ouvrir le projet

```bash
npm run start 
```

ou vous pouvez ouvrir le fichier build/index.html
