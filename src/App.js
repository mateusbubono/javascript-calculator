import React from 'react';
import $ from 'jquery';
import './App.css';

class JavaScriptCalculator extends React.Component
{
  constructor(props, state)
  {
    super(props, state)

    this.state = {
      bIsEqualProceed : false
    }

    this.clear = this.clear.bind(this);
    this.enteringNumber = this.enteringNumber.bind(this);
    this.enteringOperation = this.enteringOperation.bind(this);
    this.proceedToOperation = this.proceedToOperation.bind(this);
    this.enteringFloat = this.enteringFloat.bind(this);
  }

  clear()
  {
    $("#display-operation").text("");
    $("#display").text("0");
  }

  enteringNumber(e)
  {
    const displayContent = $("#display").text()
    const displayContentOperation = $("#display-operation").text();
    if("0+/*-".indexOf(displayContent) !== -1)
    {
      $("#display").text($("#"+e.target.id).text());
      if(displayContentOperation === "0")
      {
        $("#display-operation").text($("#"+e.target.id).text());
      }
      else 
      {
        $("#display-operation").text(displayContentOperation + $("#"+e.target.id).text());
      }
    }
    else 
    {
      if(this.state.bIsEqualProceed)
      {
        this.setState({bIsEqualProceed: false})
        $("#display-operation").text($("#"+e.target.id).text());
        $("#display").text($("#"+e.target.id).text());
      }
      else 
      {
        $("#display-operation").text(displayContentOperation + $("#"+e.target.id).text());
        $("#display").text(displayContent + $("#"+e.target.id).text());
      }
    }
  }

  enteringOperation(e)
  {
    const displayContent = $("#display").text()
    const displayContentOperation = $("#display-operation").text();
    if(!isNaN(displayContent))
    {
      if(this.state.bIsEqualProceed)
      {
        $("#display-operation").text(displayContent + $("#"+e.target.id).text());
        this.setState({bIsEqualProceed: false})
      }
      else 
      {
        $("#display-operation").text(displayContentOperation + $("#"+e.target.id).text());
      }
      $("#display").text($("#"+e.target.id).text());
      
    }
  }

  enteringFloat(e)
  {
    const displayContent = $("#display").text()
    const displayContentOperation = $("#display-operation").text();
    if(/[.]/.test(displayContent) === false)
    {
      if(/[0+\-*/]/.test(displayContent) === true)
      {
        $("#display").text("0" + $("#"+e.target.id).text());
        if(this.state.bIsEqualProceed)
        {
          this.setState({bIsEqualProceed: false})
          $("#display-operation").text("0"+ $("#"+e.target.id).text());
        }
        else
        {
          $("#display-operation").text(displayContentOperation +"0"+ $("#"+e.target.id).text());
        }
        
      }
      else 
      {
        $("#display").text(displayContent + $("#"+e.target.id).text());
        $("#display-operation").text(displayContentOperation + $("#"+e.target.id).text());
        if(this.state.bIsEqualProceed)
        {
          this.setState({bIsEqualProceed: false})
          $("#display-operation").text(displayContent+ $("#"+e.target.id).text());
        }
        else
        {
          $("#display-operation").text(displayContent+ $("#"+e.target.id).text());
        }
      }  
    }
  }

  proceedToOperation()
  {
    if(!this.state.bIsEqualProceed)
    {
      //const displayContent = $("#display").text()
      const displayContentOperation = $("#display-operation").text()
      // eslint-disable-next-line
      const result = eval(displayContentOperation) 

      $("#display").text(result);
      $("#display-operation").text(displayContentOperation + "=" + result)

      this.setState({bIsEqualProceed: true})
    }
  }

  render()
  {
    return (
      <div id="calculator">
        <div id="display-operation" className="text-warning"></div>
        <div id="display">0</div>
        <button className="btn btn-danger" id="clear" onClick={this.clear}>C</button>
        <button className="btn btn-secondary" id="divide" onClick={this.enteringOperation}>/</button>
        <button className="btn btn-secondary" id="multiply" onClick={this.enteringOperation}>*</button>

        <button className="btn btn-dark" onClick={this.enteringNumber} id="seven">7</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="eight">8</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="nine">9</button>
        <button className="btn btn-secondary" id="substract" onClick={this.enteringOperation}>-</button>

        <button className="btn btn-dark" onClick={this.enteringNumber} id="four">4</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="five">5</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="six">6</button>
        <button className="btn btn-secondary" id="add" onClick={this.enteringOperation}>+</button>

        <button className="btn btn-dark" onClick={this.enteringNumber} id="one">1</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="two">2</button>
        <button className="btn btn-dark" onClick={this.enteringNumber} id="three">3</button> 
        <button className="btn btn-primary" onClick={this.proceedToOperation} id="equals">=</button>

        <button className="btn btn-dark" onClick={this.enteringNumber} id="zero">0</button>
        <button className="btn btn-dark" id="decimal" onClick={this.enteringFloat}>.</button>
        
      </div>
    )
  }
}

function App() {
  return (
    <div className="App d-flex justify-content-center align-items-center">
      <JavaScriptCalculator/>
    </div>
  );
}

export default App;
